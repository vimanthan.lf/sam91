#!/bin/bash

# Variables
NEXUS_URL="http://192.168.1.30:8081/repository/sam91/logicfocus/com/sam91/1.0.1/sample-0.0.1-SNAPSHOT.jar"
JAR_FILE="sam91-1.0.1-plain.jar"
DESTINATION="/home/logicfocus/sam"
USERNAME="admin"
PASSWORD="lfadmin"

# Ensure the destination directory exists
mkdir -p "${DESTINATION}"

# Download the JAR file
curl -u "${USERNAME}:${PASSWORD}" -o "${DESTINATION}/${JAR_FILE}" "${NEXUS_URL}" -v

# Check if the download was successful
if [ $? -eq 0 ]; then
  echo "Download completed successfully and placed in ${DESTINATION}/${JAR_FILE}"
else
  echo "Download failed"
fi
